<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package trial
 */

get_header();
?>

    <main class="home-page" xmlns="http://www.w3.org/1999/html">
        <section class="section about-section">
            <div class="absolute-block">О нас</div>
            <div class="container">
                <h2 class="title"><?php _e('О нас ') ?></h2>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-5 wow fadeInLeft">
                            <div class="about__img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/doctor.png" alt="img">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7 wow fadeInRight">
                            <div class="about-slider">
                                <div class="about__item">
                                    <div class="about__title">
                                        <?php _e('Здоровье зубов – один из важных составляющих комфортного существования человека') ?>
                                    </div>
                                    <div class="about__text">
                                        <p>
                                            <?php _e('Стоматология Запорожья сегодня – это новейшее оборудование, инновационные
                                            методы
                                            лечения, современные препараты
                                            для обезболивания, уникальное диагностическое оборудование. Гипоаллергенные
                                            смеси для пломбирования кариеса и каналов.
                                            Безболезненное лечение пульпитов. В стоматологию Запорожья теперь не боятся
                                            ходить даже дети.') ?>
                                        </p>
                                        <p>
                                            <?php _e(' Приходите в стоматологию Запорожья, чтобы больше не бояться улыбаться,
                                            у нас есть все, что захочется без болезненных ощущений, чтобы смеяться всем
                                            неприятностям в лицо!') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section services-section">
            <div class="absolute-block"><?php _e('Услуги') ?></div>
            <div class="container">
                <h2 class="title"><?php _e('Услуги') ?></h2>

                <div class="row wow fadeInUp">
                    <?php
                    // параметры по умолчанию
                    $posts = get_posts(array(
                        'numberposts' => 8,
                        'post_type' => 'services',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($posts as $post) {
                        setup_postdata($post);
                        ?>

                        <div class="col-lg-3 col-md-6  col-sm-12">
                            <a href="<?php the_permalink() /* URL записи */ ?>" class="services">
                                <span class="services__head">
                                    <span class="services__icon">
                                        <?php the_post_thumbnail('', $default_attr); ?>
                                    </span>
                                    <span class="services__title"><?php the_title() /* выводим заголовок */ ?></span>
                                </span>
                                <span class="services__list">
                                    <?php the_excerpt(); ?>
                                </span>
                            </a>
                        </div>
                        <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-box text-center">
                            <a href="/uslugi/" class="btn-action">
                                <?php _e('подробнее') ?>
                                <span class="icon icon-arrow-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="team-section section">
            <div class="container">
                <h2 class="title"><?php _e('Наша команда') ?></h2>
                <div class="desc">
                    <?php _e('Мы - команда профессионалов, работающая с новейшим оборудованием, инновационными методами лечения,
                    современными препаратами для обезболивания, уникальным диагностическим оборудованием.') ?>
                </div>
                <div class="team-slider wow fadeInUp">

                    <?php
                    // параметры по умолчанию
                    $posts = get_posts(array(
                        'numberposts' => 1000,
                        'post_type' => 'team',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($posts as $post) {
                        setup_postdata($post);
                        ?>

                        <a href="/nasha-komanda"
                           class="team-slider__item">
                            <span class="team-slider__img">
                                <?php the_post_thumbnail('', $default_attr); ?>
                            </span>
                            <span class="team-slider-text">
                                <span class="team-slider__name">
                                    <?php the_title() /* выводим заголовок */ ?>
                                </span>
                                <span class="team-slider__desc">
                                    <?php the_excerpt(); ?>
                                </span>
                            </span>
                        </a>
                        <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>

                    <!--<div class="team-slider__item">
                        <img class="team-slider__img" src="http://trial.zp.ua/images/yasha.jpg"
                             alt="name">

                        <div class="team-slider-text">
                            <div class="team-slider__name">Зильбер Яков Аркадьевич</div>
                            <div class="team-slider__desc">Врач стоматолог-ортопед, гл. врач</div>
                        </div>
                    </div>
-->
                </div>
            </div>
        </section>

        <section class="news-home section">
            <div class="absolute-block"><?php _e('Новости') ?></div>
            <div class="container">
                <h2 class="title"><?php _e('Новости') ?></h2>
                <div class="container">
                    <div class="row wow fadeInUp">

                        <?php
                        $args = array('posts_per_page' => 4);
                        $lastposts = get_posts($args);

                        foreach ($lastposts as $post) {
                            setup_postdata($post); // устанавливаем данные
                            ?>

                            <div class="col-md-6">
                                <a href="<?php the_permalink() /* URL записи */ ?>" class="news">

                                    <span class="news__img">
                                        <?php the_post_thumbnail('', $default_attr); ?>
                                    </span>
                                    <span class="news-text">
                                        <span class="news__title"><?php the_title() /* выводим заголовок */ ?></span>
                                        <span class="news__text">
                                            <?php the_excerpt(); ?>
                                        </span>
                                    </span>
                                </a>
                            </div>


                            <?php
                        }
                        wp_reset_postdata(); // сброс
                        ?>

                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 text-center">
                            <a href="/novosti/" class="btn-action">
                                <?php _e('Все новости') ?>
                                <span class="icon icon-arrow-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section testimonials-section">
            <div class="container">
                <h2 class="title"><?php _e('Отзывы') ?></h2>
            </div>
            <div class="testimonials-content"
                 style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg/bg.png">
                <div class="container">
                    <div class="testimonials-slider wow fadeInUp">
                        <?php echo do_shortcode('[testimonial_view id="2"]'); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mt-5 text-right">
                            <a href="/komentari/" class="btn btn-dark-light">
                                <?php _e('Все отзывы') ?>
                                <span class="icon icon-arrow-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php get_template_part('parts/map'); ?>
    </main>
<?php
get_footer();
