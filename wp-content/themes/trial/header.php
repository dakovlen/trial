<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package trial
 */

?>
    <!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/assets/images/favicon.png"
              type="image/x-icon">
        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
<div class="load-container">
    <div class="loader">
        <div class="loader__text">Trial</div>
        <div class="loader__first"></div>
        <div class="loader__second"></div>
        <div class="loader__third"></div>
    </div>
</div>

<header class="header-top">
    <div class="container header-top__box">
        <div class="menu-icon-box">
            <div class="icon-menu"></div>
        </div>

        <?php wp_nav_menu([
            'container' => false,
            'theme_location' => 'menu-main'
        ]); ?>

        <?php get_template_part('parts/social_icon'); ?>

        <div class="lang">
            <?php pll_the_languages(array('display_names_as' => 1)); ?>
        </div>
    </div>
</header>

<div class="header-bottom">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-3">
                <div class="logo-box">
                    <a href="<?php echo esc_url(home_url('/')); ?>">
                        <span class="logo-link" rel="home">
                            <img class="logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo_new.png" alt="logo">
                        </span>
                        <span class="desc-box">
                            <span class="desc desc__start">Опыт поколений -</span>
                            <span class="desc desc__end">гарантия успеха</span>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tel-box">
                    <div class="tel__icon">
                        <i class="fas fa-phone"></i>
                    </div>
                    <div class="tel">
                        <a href="tel:+380612634269">+38 0612 63 42 69</a>
                        <a href="tel:+380671557155">+38 067 155 7 155</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <a href="https://goo.gl/maps/JKjyv1SafXfFFKMq8"
                   class="address-box"
                   target="_blank">
                        <span class="address__icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </span>
                    <span class="address">
                            <p class="address__text"> <?php _e('г. Запорожье,') ?></p>
                            <p class="address__text"><?php _e('ул. Александровская, 114a') ?></p>
                        </span>
                </a>
            </div>
            <div class="col-md-3">
                <div class="header-btn">
                    <a href="#"
                       class="btn-action"
                       data-toggle="modal"
                       data-target="#exampleModalCenter"><?php _e('Записаться на прием') ?></a>
                    <a href="#"
                       class="btn-action btn-light"
                       data-toggle="modal"
                       data-target="#exampleModalCenter1 "><?php _e('Online консультация') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 1 -->
<div class="modal fade"
     id="exampleModalCenter"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php _e('Записаться на прием') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <?php echo do_shortcode('[contact-form-7 id="49" title="appointment"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal 1 End -->

<!-- Modal 2 Start -->
<div class="modal fade"
     id="exampleModalCenter1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle1"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered"
         role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLongTitle1"><?php _e('Online консультация') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="write-our__item write-our__form">
                    <?php echo do_shortcode('[contact-form-7 id="50" title="online"]'); ?>
                </div>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <!--<button type="button" class="btn btn-primary">Отправить</button>-->
            </div>
        </div>
    </div>
</div>
<!-- Modal 2 End -->

<!-- Modal action start -->
<div class="modal fade"
     id="modalAction"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle1"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered"
         role="document">
        <div class="modal-content">
            <img src="<?php bloginfo('template_url'); ?>/assets/images/action/action2.png" alt="img">
        </div>
    </div>
</div>
<!-- Modal action end -->

<?php get_template_part('parts/index_slider'); ?>