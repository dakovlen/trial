<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package trial
 */

get_header();
?>
    <main id="main" class="error-container site-main">
        <section class="error-404 not-found">
            <h1> <?php _e('Ошибка 404')?></h1>
            <h2 class="title"><?php _e('К сожалению. Такой страницы не существует :(')?></h2>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="btn-action">
                <?php _e('Перейти на главную')?>
            </a>
        </section><!-- .error-404 -->
    </main><!-- #main -->
<?php
get_footer();
