<section class="index-slider">
    <div class="index-slider__item">
        <img src="<?php bloginfo('template_url'); ?>/assets/images/slider/slider.png" alt="">
        <div class="index-slider-text">
            <div class="index-slider-text__title title">
                <?php _e('"Триал" - стоматология в Запорожье: лечим зубы без слез и боли!') ?>
            </div>
            <a class="index-slider-text__btn btn-action" href="#exampleModalCenter" data-toggle="modal">
                <?php _e('Записаться на прием') ?>
            </a>
        </div>
    </div>

    <div class="index-slider__item">
        <img src="<?php bloginfo('template_url'); ?>/assets/images/slider/slider.png" alt="">
        <div class="index-slider-text">
            <div class="index-slider-text__title title">
                <?php _e('"Триал" - стоматология в Запорожье: лечим зубы без слез и боли!') ?>
            </div>
            <a class="index-slider-text__btn btn-action" href="#exampleModalCenter" data-toggle="modal">
                <?php _e('Записаться на прием') ?>
            </a>
        </div>
    </div>
</section>