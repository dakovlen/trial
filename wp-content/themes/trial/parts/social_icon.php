<div class="social">
    <a href="https://www.facebook.com/trial.zp"
       class="social__link"
       target="_blank">
        <i class="social__icon fab fa-facebook-f"></i>
    </a>
    <a href="https://www.instagram.com/trial.zp"
       class="social__link"
       target="_blank">
        <i class="social__icon fab fa-instagram"></i>
    </a>
    <a href="https://twitter.com/TrialZp"
       class="social__link"
       target="_blank">
        <i class="social__icon fab fa-twitter"></i>
    </a>
</div>