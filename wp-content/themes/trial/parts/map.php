<div class="map">
    <div class="contact-info">
        <div class="address">
            <p class="address__text"> <?php _e('г. Запорожье,')?></p>
            <p class="address__text"><?php _e('ул. Александровская, 114a')?></p>
        </div>
        <div class="tel">
            <a href="tel:+380612634269">+38 0612 63 42 69</a>
            <a href="tel:+380671557155">+38 067 155 7 155</a>
            <a href="mailto:trial.stomat@gmail.com">E-mail: trial.stomat@gmail.com</a>
        </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10714.778014208601!2d35.1698398!3d47.8261374!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3d0e8b24cac9d26!2sTrial!5e0!3m2!1sru!2sua!4v1581607166332!5m2!1sru!2sua"
            width="100%"
            height="450"
            frameborder="0"
            style="border:0;"
            allowfullscreen=""
    ></iframe>
</div>