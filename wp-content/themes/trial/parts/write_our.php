<section class="write-our-section section">
    <div class="container">
        <div class="absolute-block">Напишите нам</div>
        <h2 class="title"><?php _e('Остались вопросы') ?></h2>
        <div class="desc"><?php _e('Напишите нам!') ?></div>
        <div class="write-our wow fadeInUp">
            <div class="row">
                <div class="col-md-4">
                    <img class=" write-our__img" src="<?php bloginfo('template_url'); ?>/assets/images/contact.png"
                         alt="img">
                </div>

                <div class="col-md-7 write-our__item write-our__form">
                    <?php echo do_shortcode('[contact-form-7 id="48" title="Contact form 1"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>