<?php

/**
 * Template Name: Services page
 *
 * @package     WordPress
 * @subpackage  RST v3
 * @since       1.0.0
 * @author      Koval
 */

?>

<?php

/**
 * Include header.php or header-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_header
 */
get_header();
?>
    <section class="services-container section">
        <div class="container">
            <div class="container">
                <h2 class="title"><?php _e('Услуги')?></h2>
                <div class="row wow fadeInUp">
                    <?php
                    // параметры по умолчанию
                    $posts = get_posts(array(
                        'numberposts' => 8,
                        'post_type' => 'services',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($posts as $post) {
                        setup_postdata($post);
                        ?>

                        <div class="col-lg-3 col-md-6  col-sm-12">
                            <a href="<?php the_permalink()?>" class="services">
                                <span class="services__head">
                                    <span class="services__icon">
                                        <?php the_post_thumbnail('', $default_attr); ?>
                                    </span>
                                    <span class="services__title"><?php the_title() /* выводим заголовок */ ?></span>
                                </span>

                                <span class="services__list">
                                    <span>
                                        <?php the_excerpt(); ?>
                                    </span>

                                    <span class="btn btn-link mt-4">
                                        <?php _e('Подробнее')?>
                                        <span class="icon icon-arrow-right"></span>
                                    </span>
                                </span>
                            </a>
                        </div>
                        <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php

/**
 * Include footer.php of footer-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_footer
 */
get_footer();

?>