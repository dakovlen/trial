<?php

/**
 * Template Name: About page
 *
 * @package     WordPress
 * @subpackage  RST v3
 * @since       1.0.0
 * @author      Koval
 */

?>

<?php

/**
 * Include header.php or header-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_header
 */
get_header();
?>
    <section class="about-container section">
        <div class="container">
            <h2 class="title"><?php the_title() ?></h2>
            <?php
            the_post();
            the_content()
            ?>
        </div>
    </section>
<?php

/**
 * Include footer.php of footer-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_footer
 */
get_footer();

?>