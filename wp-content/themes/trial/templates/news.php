<?php

/**
 * Template Name: News page
 *
 * @package     WordPress
 * @subpackage  RST v3
 * @since       1.0.0
 * @author      Koval
 */

get_header();
?>
    <section class="all-news section">
        <div class="container">
            <h2 class="title mb-5"><?php _e('Новости')?></h2>
            <?php
            // запрос
            $wpb_all_query = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => -1)); ?>

            <?php if ($wpb_all_query->have_posts()) : ?>

                <!-- the loop -->
                <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); ?>

                    <a class="news row" href="<?php the_permalink(); ?>">
                        <span class="col-md-3 news__img">
                            <?php the_post_thumbnail(''); ?>
                        </span>
                        <span class=" col-md-9 news-text">
                            <span class="news__title"><?php the_title() ?></span>
                            <span class="news__text"><?php the_excerpt(); ?></span>
                        </span>
                    </a>

                <?php endwhile; ?>
                <!-- end of the loop -->


                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php _e('Извините, нет записей, соответствуюших Вашему запросу.'); ?></p>
            <?php endif; ?>
            <?php wp_pagenavi(); ?>

        </div>
    </section>

<?php

/**
 * Include footer.php of footer-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_footer
 */
get_footer();

?>