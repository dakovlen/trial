<?php

/**
 * Template Name: Contact page
 *
 * @package     WordPress
 * @subpackage  RST v3
 * @since       1.0.0
 * @author      Koval
 */

?>

<?php

/**
 * Include header.php or header-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_header
 */
get_header();
?>
    <section class="services-container section">
        <div class="container">
            <h2 class="title mb-5"><?php _e('Контакты')?></h2>
        </div>
        <?php get_template_part('parts/map'); ?>
        <div class="contact-social">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-center"><?php _e('Подписывайтесь на нас в соц сетях!')?></h3>
                        <?php get_template_part('parts/social_icon'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php

/**
 * Include footer.php of footer-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_footer
 */
get_footer();

?>