<?php

/**
 * Template Name: Comments page
 *
 * @package     WordPress
 * @subpackage  RST v3
 * @since       1.0.0
 * @author      Koval
 */

?>
<?php get_header(); ?>
    <section class="section-testimonials">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo do_shortcode('[testimonial_view id="3"]'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php echo do_shortcode('[testimonial_view id="1"]'); ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>