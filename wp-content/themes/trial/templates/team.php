<?php

/**
 * Template Name: Team page
 *
 * @package     WordPress
 * @subpackage  RST v3
 * @since       1.0.0
 * @author      Koval
 */

?>

<?php

/**
 * Include header.php or header-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_header
 */
get_header();
?>
    <section class="services-container section">
        <div class="container">
            <h2 class="title"><?php _e('Наша команда') ?></h2>
            <h5 class="sub-title text-center mb-4">
                <?php _e('Мы - команда профессионалов, работающая с новейшим оборудованием, инновационными
                методами лечения, современными препаратами для обезболивания, уникальным диагностическим
                оборудованием.') ?>
            </h5>

            <div class="row">

                <?php
                // параметры по умолчанию
                $posts = get_posts(array(
                    'numberposts' => 1000,
                    'post_type' => 'team',
                    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ));

                foreach ($posts as $post) {
                    setup_postdata($post);
                    ?>

                    <div class="col-md-3">
                        <div class="team">
                            <div class="team__img">
                                <?php the_post_thumbnail('', $default_attr); ?>
                            </div>
                            <div class="team__name"><?php the_title() /* выводим заголовок */ ?></div>
                            <div class="team__position"><?php the_excerpt(); ?></div>
                        </div>
                    </div>
                    <?php
                }

                wp_reset_postdata(); // сброс
                ?>
            </div>
        </div>
    </section>
<?php

/**
 * Include footer.php of footer-XXX.php for custom page
 *
 * @link        https://codex.wordpress.org/Function_Reference/get_footer
 */
get_footer();

?>