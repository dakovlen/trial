(function ($) {
    $(document).ready(function () {
        //MENU RESPONSIVE
        $('.menu-icon-box').click(function () {
            $('.menu').toggleClass('open');
            $(this).toggleClass('open');
        });

        //INDEX SLIDER
        $('.index-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3500,
            dots: true,
            arrows: false
        });

        //TEAM SLIDER
        $('.team-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            initialSlide: 1,
            pauseOnHover: true,
            infinite: true,
            centerMode: true,
            dots: false,
            nextArrow: '<button type="button" class="slick-next"><span class="icon icon-arrow-right"></span></button>',
            prevArrow: '<button type="button" class="slick-prev"><span class="icon icon-arrow-right"></span></button>',
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        //TEAM SLIDER
        $('.about-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: false,
            arrows: true
        });

        //TESTIMONIALS SLIDER
        $('.testimonials-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: true
        });

        new WOW().init();
    });
    //text radius

    $('.header-bottom .desc, .footer-content .desc').arctext({radius: 500});

    //PRELOADER
    function preloader() {
        $(()=> {
            setInterval(()=> {
                let p = $('.load-container');
                p.css('opacity', 0);

                setInterval(
                    () => p.remove(),
                    parseInt(p.css('$transition')) * 1000
                );

            }, 1000);
        });
    }

    preloader()

    $(document).ready(function(){
        $('#modalAction').modal('show');
    });

    // declare variable
    var scrollTop = $(".scrollTop");

    $(window).scroll(function() {
        // declare variable
        var topPos = $(this).scrollTop();

        // if user scrolls down - show scroll to top button
        if (topPos > 100) {
            $(scrollTop).css("opacity", "1");

        } else {
            $(scrollTop).css("opacity", "0");
        }

    }); // scroll END

    //Click event to scroll to top
    $(scrollTop).click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;

    }); // click() scroll top END

})(jQuery);
