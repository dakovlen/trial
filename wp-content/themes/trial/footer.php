<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package trial
 */

?>
<?php get_template_part('parts/write_our'); ?>
<footer class="footer-content">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo-box">
                    <a href="<?php echo esc_url(home_url('/')); ?>">
                        <span class="logo-link" rel="home">
                            <img class="logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo_new.png" alt="logo">
                        </span>
                        <span class="desc-box">
                            <span class="desc desc__start">Опыт поколений -</span>
                            <span class="desc desc__end">гарантия успеха</span>
                        </span>
                    </a>
                </div>
            </div>

            <div class="col-md-8">
                <div class="footer-menu">
                    <?php wp_nav_menu([
                        'container' => false,
                        'theme_location' => 'menu-main'
                    ]); ?>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-6">
                        <?php get_template_part('parts/social_icon'); ?>
                    </div>
                    <div class="col-md-4">
                        <div class="tel-box">
                            <div class="tel__icon">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="tel">
                                <a href="tel:0612634269">0612 63 42 69</a>
                                <a href="tel:0671557155">067 155 7 155</a>
                            </div>
                        </div>
                    </div>

                    <!--<div class="col-md-6">
                        <a href="#" class="address-box">
                            <span class="address__icon">
                                <i class="fas fa-map-marker-alt"></i>
                            </span>
                            <span class="address">
                                <span class="address__text"> г. Запорожье,</span>
                                <span class="address__text">ул. Александровская, 114a</span>
                            </span>
                        </a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="stop" class="scrollTop">
    <span>
        <a href="" class="scroll__icon fas fa-chevron-circle-up"></a>
    </span>
</div>

<?php wp_footer(); ?>

</body>
</html>
