FROM php:7.3-fpm

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt update \
    && apt install -y --no-install-recommends \
        default-mysql-client \
        git \
        libfreetype6-dev \
        libjpeg-dev \
        libmagickwand-dev \
        libpng-dev \
        libssl-dev \
        libxml2-dev \
        libzip-dev \
        nodejs \
        unzip \
        vim \
        zip \
    && docker-php-ext-install bcmath exif gd mysqli opcache zip > /dev/null \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp \
    && apt clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
